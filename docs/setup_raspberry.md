# Deploiement 

## notes
* external link new tab  

## raspberry pi

### img
#### Hostname

```
inflor-x
```

### Raspberry pi Imager

#### 32 bits image default

### Post-Flash config

#### rm ~/*flafla

```
rm -rf Bookshelf/ Music/ Public/ Templates/  Pictures/ 

```
#### Update upgrade

```
sudo apt update && sudo apt upgrade
```

#### apt get minimum
```
sudo apt install tmux micro

```

#### Sources = ~/src

```
mkdir ~/src && cd ~/src
```

#### Screen Drivers 

##### Waveshare 

###### Refs
* [wiki/10.1inch_DSI_LCD_(C)](https://www.waveshare.com/wiki/10.1inch_DSI_LCD_(C))

* [product/10.1inch_DSI_LCD_(C)](https://www.waveshare.com/wiki/10.1inch_DSI_LCD_(C))

###### Install Driver 
```
cd ~/src
git clone https://github.com/waveshare/Waveshare-DSI-LCD
```

```bash
~/src/Waveshare-DSI-LCD/5.15.84/32/WS_xinchDSI_MAIN.sh 101C I2C0
```
###### If fail 

```bash
cd ~/src/Waveshare-DSI-LCD
```

* kernel version ?

```bash
uname -a 
```

``` bash
cd 5.15.84
```
* archictecture ?

```bash
getconf LONG_BIT
```

```bash
cd 32
```

* setup screen driver 

```bash
sudo bash ./WS_xinchDSI_MAIN.sh 101C I2C0
```
###### endif

##### Set Screen Brightness

###### FULL
```bash
sudo su 
echo 0 > /sys/waveshare/rpi_backlight/brightness 
exit
```
###### Disable
```bash
sudo su 
echo 255 > /sys/waveshare/rpi_backlight/brightness
exit
```



#### /boot/config.txt


```
# Disable touch 
disable_touchscreen=1
```

```
# USB C HOST
dtoverlay=dwc2,dr_mode=host
```
 

##### POE fan

```
# PoE Hat Fan Speeds
dtparam=poe_fan_temp0=50000
dtparam=poe_fan_temp1=60000
dtparam=poe_fan_temp2=70000
dtparam=poe_fan_temp3=80000
```


#### Paquets APT

```bash
sudo apt install  python3-pip micro git tmux vlc 
```
#### Paquets Python

```bash
pip install python-vlc python-osc
```

#### Networking

##### Samba 

```bash
sudo apt-get update  && \
sudo apt-get install samba samba-common-bin && \
sudo smbpasswd -a pi 
sudo service smbd restart

```

###### i/o ~/

```
micro /etc/samba/smb.conf 
```

@~line 176

```diff
- read only = yes
+ read only = no
```

```bash
sudo systemctl restart smbd.service 
```



#### Avahi&Bonjour SSH

```bash
sudo su
curl https://raw.githubusercontent.com/lathiat/avahi/master/avahi-daemon/ssh.service > /etc/avahi/services/ssh.service
exit
sudo systemctl restart avahi-daemon.service 

```

#### Config Git

```bash
git config --global user.email "$USER@$HOSTNAME.local" && \
git config --global user.name "$HOSTNAME"
```

#### Config SSH KEY 



##### ! SSH KEY

```
ssh-keygen -t rsa -b 2048 -C "$HOSTNAME"

```

### Puredata 

```bash
sudo apt-get install autoconf libtool libasound2-dev build-essential autoconf automake libtool gettext git libasound2-dev libjack-jackd2-dev libfftw3-3 libfftw3-dev tcl tk && \
cd ~/src && \
git clone https://github.com/pure-data/pure-data && \
cd pure-data && \
./autogen.sh && \
./configure --enable-jack --enable-fftw && \
make -j4 && \
sudo make install
```

### Recurse Clone infloresence 

``` bash
cd ~/src \
git clone --recurse-submodules git@gitlab.com:_inflorescence/inflorescence.git

```

### OLA

```
sudo apt-get install libcppunit-dev libcppunit-1.15-0 uuid-dev pkg-config libncurses5-dev libtool autoconf automake g++ libmicrohttpd-dev libmicrohttpd12 protobuf-compiler libprotobuf-lite23 libprotobuf-dev libprotoc-dev zlib1g-dev bison flex make libftdi-dev libftdi1 libusb-1.0-0-dev liblo-dev libavahi-client-dev 

```


```
pip install protobuf numpy
```


```
cd ~/src
git clone https://github.com/OpenLightingProject/ola
cd ola
autoreconf -i
./configure
make -j4
make check
sudo make install
sudo ldconfig
```

```
pip python-protobuf python-numpy
```